package com.noon.hackathon.bitsnbytes.service;

import com.noon.hackathon.bitsnbytes.commands.TrendingCommandExecutionHelper;
import com.noon.hackathon.bitsnbytes.config.LittlebirdsConfig;
import com.noon.hackathon.bitsnbytes.dao.DailyTrendingInfoDao;
import com.noon.hackathon.bitsnbytes.dao.HourlyTrendingInfoDao;
import com.noon.hackathon.bitsnbytes.dto.DailyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.dto.HourlyTrendingInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
@Service
public class TrendingInfoService {
    @Autowired
    DailyTrendingInfoDao dailyTrendingInfoDao;

    @Autowired
    HourlyTrendingInfoDao hourlyTrendingInfoDao;

    @Autowired
    LittlebirdsConfig littlebirdsConfig;

    @Autowired
    TrendingCommandExecutionHelper trendingCommandExecutionHelper;

    public void createDailyTrendingInfo(DailyTrendingInfoDTO dailyTrendingInfoDTO) {
        dailyTrendingInfoDao.save(dailyTrendingInfoDTO);
    }

    public void createHourlyTrendingInfo(HourlyTrendingInfoDTO hourlyTrendingInfoDTO) {
        hourlyTrendingInfoDao.save(hourlyTrendingInfoDTO);
    }

    public Map<String, Map<String, Object>> getTrendingInfo(int countryId) {
        Map<String, Map<String, Object>> resultMap = new HashMap<>();

        if (!littlebirdsConfig.getCountryWiseTrendingInfo().containsKey(countryId)) {
            return resultMap;
        }

        littlebirdsConfig.getCountryWiseTrendingInfo().get(countryId).forEach(trendingInfoCommand -> {
            resultMap.putAll(trendingCommandExecutionHelper.executeCommand(countryId, trendingInfoCommand));
        });

        return resultMap;
    }
}
