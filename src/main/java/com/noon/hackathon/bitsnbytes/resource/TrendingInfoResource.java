package com.noon.hackathon.bitsnbytes.resource;

import com.noon.hackathon.bitsnbytes.commands.HourlyTrendingCommand;
import com.noon.hackathon.bitsnbytes.dto.DailyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.dto.HourlyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.service.TrendingInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
@Slf4j
@RestController
@RequestMapping("/v1/trending")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TrendingInfoResource {
    @Autowired
    TrendingInfoService trendingInfoService;

    @PostMapping(value = "/daily")
    public ResponseEntity saveDailyTrendingInfo(@RequestBody DailyTrendingInfoDTO dailyTrendingInfoDTO) {
        trendingInfoService.createDailyTrendingInfo(dailyTrendingInfoDTO);

        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @PostMapping(value = "/hourly")
    public ResponseEntity saveHourlyTrendingInfo(@RequestBody HourlyTrendingInfoDTO hourlyTrendingInfoDTO) {
        trendingInfoService.createHourlyTrendingInfo(hourlyTrendingInfoDTO);

        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @GetMapping(value = "/{country_id}")
    public ResponseEntity getCountryTrendingInfo(@PathVariable("country_id") int countryId) {
        Map<String, Map<String, Object>> trendingInfo = trendingInfoService.getTrendingInfo(countryId);

        return ResponseEntity.status(HttpStatus.OK).body(trendingInfo);
    }
}
