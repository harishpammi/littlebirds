package com.noon.hackathon.bitsnbytes.commands;

import com.noon.hackathon.bitsnbytes.dto.TrendingCategory;

import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
public interface BaseTrendingCommand {
    Map<String, Object> execute(int countryId, TrendingCategory trendingCategory);
}
