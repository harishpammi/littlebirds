package com.noon.hackathon.bitsnbytes.commands;

import com.noon.commons.mapper.MapperUtil;
import com.noon.hackathon.bitsnbytes.dao.DailyTrendingInfoDao;
import com.noon.hackathon.bitsnbytes.dto.DailyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.dto.TrendingCategory;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
@Service
public class DailyTrendingCommand implements BaseTrendingCommand {
    @Autowired
    DailyTrendingInfoDao dailyTrendingInfoDao;

    @Override
    public Map<String, Object> execute(int countryId, TrendingCategory trendingCategory) {
        //Date date = new Date();
        Date date = DateUtils.addDays(new Date(), -1);

        List<DailyTrendingInfoDTO> dailyTrendingItemInfoList = dailyTrendingInfoDao.findDailyTrendingItemInfo(countryId, trendingCategory.getName(), date);
        if (CollectionUtils.isEmpty(dailyTrendingItemInfoList)) {
            return new HashMap<>();
        }

        DailyTrendingInfoDTO dailyTrendingInfoDTO = dailyTrendingItemInfoList.get(0);

        return MapperUtil.convertStringToMap(MapperUtil.convertObjectToString(dailyTrendingInfoDTO));
    }
}
