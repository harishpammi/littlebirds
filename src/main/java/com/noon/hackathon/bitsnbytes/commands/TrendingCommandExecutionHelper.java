package com.noon.hackathon.bitsnbytes.commands;

import com.noon.hackathon.bitsnbytes.dto.TrendingCategory;
import com.noon.hackathon.bitsnbytes.dto.TrendingInfoCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
@Service
public class TrendingCommandExecutionHelper {
    @Autowired
    DailyTrendingCommand dailyTrendingCommand;

    @Autowired
    HourlyTrendingCommand hourlyTrendingCommand;

    public Map<String, Map<String, Object>> executeCommand(int countryId, TrendingInfoCommand trendingInfoCommand) {
        Map<String, Map<String, Object>> resultMap = new HashMap<>();
        switch (trendingInfoCommand) {
            case DAILY_TRENDING_GROUP:
                resultMap.put("last_day_trending_group", dailyTrendingCommand.execute(countryId, TrendingCategory.GROUPS));
                break;

            case HOURLY_TRENDING_GROUP:
                resultMap.put("last_hour_trending_group", hourlyTrendingCommand.execute(countryId, TrendingCategory.GROUPS));
                break;

            case DAILY_TRENDING_TEACHER:
                resultMap.put("last_day_trending_teacher", dailyTrendingCommand.execute(countryId, TrendingCategory.TEACHERS));
                break;

            case HOURLY_TRENDING_TEACHER:
                resultMap.put("last_hour_trending_teacher", hourlyTrendingCommand.execute(countryId, TrendingCategory.TEACHERS));
                break;
        }

        return resultMap;
    }
}
