package com.noon.hackathon.bitsnbytes.commands;

import com.noon.commons.mapper.MapperUtil;
import com.noon.hackathon.bitsnbytes.dao.HourlyTrendingInfoDao;
import com.noon.hackathon.bitsnbytes.dto.HourlyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.dto.TrendingCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
@Service
public class HourlyTrendingCommand implements BaseTrendingCommand {
    @Autowired
    HourlyTrendingInfoDao hourlyTrendingInfoDao;

    @Override
    public Map<String, Object> execute(int countryId, TrendingCategory trendingCategory) {
        Date date = new Date();
        //Date date = DateUtils.addDays(new Date(), -1);
        int lastHour = LocalDateTime.now().getHour() - 1;
        List<HourlyTrendingInfoDTO> hourlyTrendingItemInfoList = hourlyTrendingInfoDao.findDailyTrendingItemInfo(countryId, trendingCategory.getName(), date, lastHour);
        if (CollectionUtils.isEmpty(hourlyTrendingItemInfoList)) {
            return new HashMap<>();
        }

        HourlyTrendingInfoDTO hourlyTrendingInfoDTO = hourlyTrendingItemInfoList.get(0);

        return MapperUtil.convertStringToMap(MapperUtil.convertObjectToString(hourlyTrendingInfoDTO));
    }
}
