package com.noon.hackathon.bitsnbytes.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-29
 */
@Data
@NoArgsConstructor
public class DailyTrendingInfoDTO {
    private int countryId;

    private TrendingCategory trendingCategory;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date trendingDate;

    private Long trendingItemId;

    private Map trendingItemDetails;

    @Builder
    public DailyTrendingInfoDTO(int countryId, TrendingCategory trendingCategory, Date trendingDate, Long trendingItemId, Map trendingItemDetails) {
        this.countryId = countryId;
        this.trendingCategory = trendingCategory;
        this.trendingDate = trendingDate;
        this.trendingItemId = trendingItemId;
        this.trendingItemDetails = trendingItemDetails;
    }
}
