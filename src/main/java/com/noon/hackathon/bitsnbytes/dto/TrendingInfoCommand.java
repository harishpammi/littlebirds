package com.noon.hackathon.bitsnbytes.dto;

import lombok.Getter;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
@Getter
public enum TrendingInfoCommand {
    HOURLY_TRENDING_GROUP("HOURLY_TRENDING_GROUP"),
    DAILY_TRENDING_GROUP("DAILY_TRENDING_GROUP"),
    HOURLY_TRENDING_TEACHER("HOURLY_TRENDING_TEACHER"),
    DAILY_TRENDING_TEACHER("DAILY_TRENDING_TEACHER");

    private String name;

    TrendingInfoCommand(String name) {
        this.name = name;
    }
}
