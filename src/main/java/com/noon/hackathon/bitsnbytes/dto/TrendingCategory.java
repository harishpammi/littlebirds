package com.noon.hackathon.bitsnbytes.dto;

import lombok.Getter;

/**
 * Created by hareesh.pammi on 2020-05-29
 */
@Getter
public enum TrendingCategory {
    GROUPS("GROUPS"), TEACHERS("TEACHERS");

    private String name;

    TrendingCategory(String name) {
        this.name = name;
    }
}
