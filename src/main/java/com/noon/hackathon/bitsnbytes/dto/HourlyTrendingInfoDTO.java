package com.noon.hackathon.bitsnbytes.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-29
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class HourlyTrendingInfoDTO extends DailyTrendingInfoDTO {
    private int trendingHour;

    public HourlyTrendingInfoDTO(int countryId, TrendingCategory trendingCategory, Date trendingDate, Long trendingItemId, Map trendingItemDetails, int trendingHour) {
        super(countryId, trendingCategory, trendingDate, trendingItemId, trendingItemDetails);
        this.trendingHour = trendingHour;
    }
}
