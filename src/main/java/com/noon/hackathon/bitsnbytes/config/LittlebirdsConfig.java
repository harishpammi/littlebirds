package com.noon.hackathon.bitsnbytes.config;

import com.noon.commons.config.MysqlConfig;
import com.noon.commons.mysql.MysqlJDBCConnectionPool;
import com.noon.commons.mysql.MysqlJDBCTemplate;
import com.noon.hackathon.bitsnbytes.dao.DailyTrendingInfoDao;
import com.noon.hackathon.bitsnbytes.dao.HourlyTrendingInfoDao;
import com.noon.hackathon.bitsnbytes.dto.TrendingInfoCommand;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-29
 */
@Data
@Configuration
@ConfigurationProperties("littlebirds-config")
public class LittlebirdsConfig {
    private MysqlConfig mysqlConfig;

    private Map<Integer, List<TrendingInfoCommand>> countryWiseTrendingInfo;

    @Bean
    public MysqlJDBCConnectionPool getMysqlJDBCConnectionPool() {
        return new MysqlJDBCConnectionPool(new MysqlJDBCTemplate(mysqlConfig), new MysqlJDBCTemplate(mysqlConfig));
    }

    @Bean
    public DailyTrendingInfoDao getDailyTrendingInfoDao(MysqlJDBCConnectionPool mysqlJDBCConnectionPool) {
        return new DailyTrendingInfoDao(mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate());
    }

    @Bean
    public HourlyTrendingInfoDao getHourlyTrendingInfoDao(MysqlJDBCConnectionPool mysqlJDBCConnectionPool) {
        return new HourlyTrendingInfoDao(mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate());
    }
}
