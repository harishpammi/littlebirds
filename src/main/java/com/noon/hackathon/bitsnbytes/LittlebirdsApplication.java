package com.noon.hackathon.bitsnbytes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by hareesh.pammi on 2020-05-29
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.noon.hackathon.bitsnbytes"})
public class LittlebirdsApplication {
    public static void main(String[] args) {
        SpringApplication.run(LittlebirdsApplication.class, args);
    }
}
