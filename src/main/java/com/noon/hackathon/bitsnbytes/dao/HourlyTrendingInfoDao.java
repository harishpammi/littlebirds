package com.noon.hackathon.bitsnbytes.dao;

import com.noon.commons.mapper.MapperUtil;
import com.noon.commons.mysql.MysqlBaseDaoImpl;
import com.noon.commons.mysql.query.FindByQuery;
import com.noon.commons.mysql.query.InsertQuery;
import com.noon.commons.mysql.query.InsertQueryReturnId;
import com.noon.commons.mysql.query.UpdateQuery;
import com.noon.hackathon.bitsnbytes.dto.HourlyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.dto.TrendingCategory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-30
 */
public class HourlyTrendingInfoDao extends MysqlBaseDaoImpl<Long, HourlyTrendingInfoDTO> {
    public static final String FIND_TRENDING_BY_DATE_QUERY="SELECT id, country_id, trending_category, trending_date, trending_hour, trending_item_id, trending_item_details FROM hourly_trending_info WHERE country_id=:country_id AND trending_category=:trending_category AND trending_date=:trending_date AND trending_hour=:trending_hour";

    public static final String INSERT_QUERY = "INSERT INTO hourly_trending_info (country_id, trending_category, trending_date, trending_hour, trending_item_id, trending_item_details) VALUES (?, ?, ?, ?, ?, ?)";

    public HourlyTrendingInfoDao(JdbcTemplate jdbcTemplate, JdbcTemplate slaveJdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate, NamedParameterJdbcTemplate slaveNamedParameterJdbcTemplate) {
        super(jdbcTemplate, slaveJdbcTemplate, namedParameterJdbcTemplate, slaveNamedParameterJdbcTemplate);
    }

    @Override
    protected InsertQuery getInsertQuery(HourlyTrendingInfoDTO hourlyTrendingInfoDTO) {
        PreparedStatementCallback<Boolean> statementCallback = statement -> {
            Integer index = 1;
            statement.setInt(index++, hourlyTrendingInfoDTO.getCountryId());
            statement.setString(index++, hourlyTrendingInfoDTO.getTrendingCategory().getName());
            statement.setDate(index++, new Date(hourlyTrendingInfoDTO.getTrendingDate().getTime()));
            statement.setInt(index++, hourlyTrendingInfoDTO.getTrendingHour());
            statement.setLong(index++, hourlyTrendingInfoDTO.getTrendingItemId());
            statement.setObject(index++, MapperUtil.convertObjectToString(hourlyTrendingInfoDTO.getTrendingItemDetails()));

            return statement.execute();
        };
        return new InsertQuery(INSERT_QUERY, statementCallback);
    }

    @Override
    protected UpdateQuery getUpdateQuery(HourlyTrendingInfoDTO hourlyTrendingInfoDTO) {
        return null;
    }

    @Override
    protected InsertQueryReturnId getInsertQueryReturnId(HourlyTrendingInfoDTO hourlyTrendingInfoDTO) {
        return null;
    }

    @Override
    protected FindByQuery getFindByQuery(Long aLong) {
        return null;
    }

    @Override
    protected FindByQuery getSelectAllQuery(Long aLong) {
        return null;
    }

    public List<HourlyTrendingInfoDTO> findDailyTrendingItemInfo(int countryId, String trendingCategory, java.util.Date trendingDate, int trendingHour) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Map<String, Object> params = new HashMap<>();
        params.put("country_id", countryId);
        params.put("trending_category", trendingCategory);
        params.put("trending_date", simpleDateFormat.format(trendingDate));
        params.put("trending_hour", trendingHour);

        return namedParameterJdbcTemplate.query(FIND_TRENDING_BY_DATE_QUERY, params, getRowMapper());
    }

    @Override
    protected RowMapper<HourlyTrendingInfoDTO> getRowMapper() {
        return new RowMapper<HourlyTrendingInfoDTO>() {
            @Override
            public HourlyTrendingInfoDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                Map trendingItemDetails = new HashMap();
                if (resultSet.getObject("trending_item_details") != null) {
                    trendingItemDetails = MapperUtil.convertStringToMap(resultSet.getString("trending_item_details"));
                }

                return new HourlyTrendingInfoDTO(
                        resultSet.getInt("country_id"),
                        TrendingCategory.valueOf(resultSet.getString("trending_category")),
                        resultSet.getDate("trending_date"),
                        resultSet.getLong("trending_item_id"),
                        trendingItemDetails,
                        resultSet.getInt("trending_hour")
                );
            }
        };
    }
}
