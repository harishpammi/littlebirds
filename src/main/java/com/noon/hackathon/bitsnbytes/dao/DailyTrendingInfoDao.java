package com.noon.hackathon.bitsnbytes.dao;

import com.noon.commons.mapper.MapperUtil;
import com.noon.commons.mysql.MysqlBaseDaoImpl;
import com.noon.commons.mysql.query.FindByQuery;
import com.noon.commons.mysql.query.InsertQuery;
import com.noon.commons.mysql.query.InsertQueryReturnId;
import com.noon.commons.mysql.query.UpdateQuery;
import com.noon.hackathon.bitsnbytes.dto.DailyTrendingInfoDTO;
import com.noon.hackathon.bitsnbytes.dto.TrendingCategory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hareesh.pammi on 2020-05-29
 */
public class DailyTrendingInfoDao extends MysqlBaseDaoImpl<Long, DailyTrendingInfoDTO> {
    public static final String FIND_TRENDING_BY_DATE_QUERY="SELECT id, country_id, trending_category, trending_date, trending_item_id, trending_item_details FROM daily_trending_info WHERE country_id=:country_id AND trending_category=:trending_category AND trending_date=:trending_date";

    public static final String INSERT_QUERY = "INSERT INTO daily_trending_info (country_id, trending_category, trending_date, trending_item_id, trending_item_details) VALUES (?, ?, ?, ?, ?)";

    public DailyTrendingInfoDao(JdbcTemplate jdbcTemplate, JdbcTemplate slaveJdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate, NamedParameterJdbcTemplate slaveNamedParameterJdbcTemplate) {
        super(jdbcTemplate, slaveJdbcTemplate, namedParameterJdbcTemplate, slaveNamedParameterJdbcTemplate);
    }

    @Override
    protected InsertQuery getInsertQuery(DailyTrendingInfoDTO dailyTrendingInfoDTO) {
        PreparedStatementCallback<Boolean> statementCallback = statement -> {
            Integer index = 1;
            statement.setInt(index++, dailyTrendingInfoDTO.getCountryId());
            statement.setString(index++, dailyTrendingInfoDTO.getTrendingCategory().getName());
            statement.setDate(index++, new Date(dailyTrendingInfoDTO.getTrendingDate().getTime()));
            statement.setLong(index++, dailyTrendingInfoDTO.getTrendingItemId());
            statement.setObject(index++, MapperUtil.convertObjectToString(dailyTrendingInfoDTO.getTrendingItemDetails()));

            return statement.execute();
        };
        return new InsertQuery(INSERT_QUERY, statementCallback);
    }

    @Override
    protected UpdateQuery getUpdateQuery(DailyTrendingInfoDTO dailyTrendingInfoDTO) {
        return null;
    }

    @Override
    protected InsertQueryReturnId getInsertQueryReturnId(DailyTrendingInfoDTO dailyTrendingInfoDTO) {
        return null;
    }

    @Override
    protected FindByQuery getFindByQuery(Long aLong) {
        return null;
    }

    @Override
    protected FindByQuery getSelectAllQuery(Long aLong) {
        return null;
    }

    public List<DailyTrendingInfoDTO> findDailyTrendingItemInfo(int countryId, String trendingCategory, java.util.Date trendingDate) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Map<String, Object> params = new HashMap<>();
        params.put("country_id", countryId);
        params.put("trending_category", trendingCategory);
        params.put("trending_date", simpleDateFormat.format(trendingDate));

        return namedParameterJdbcTemplate.query(FIND_TRENDING_BY_DATE_QUERY, params, getRowMapper());
    }

    @Override
    protected RowMapper<DailyTrendingInfoDTO> getRowMapper() {
        return new RowMapper<DailyTrendingInfoDTO>() {
            @Override
            public DailyTrendingInfoDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                Map trendingItemDetails = new HashMap();
                if (resultSet.getObject("trending_item_details") != null) {
                    trendingItemDetails = MapperUtil.convertStringToMap(resultSet.getString("trending_item_details"));
                }

                return DailyTrendingInfoDTO.builder()
                        .countryId(resultSet.getInt("country_id"))
                        .trendingCategory(TrendingCategory.valueOf(resultSet.getString("trending_category")))
                        .trendingDate(resultSet.getDate("trending_date"))
                        .trendingItemId(resultSet.getLong("trending_item_id"))
                        .trendingItemDetails(trendingItemDetails)
                        .build();
            }
        };
    }
}
