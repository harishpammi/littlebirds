#!/bin/bash

java -XX:+UseG1GC -Xmx2g -Xms256m -XX:NewRatio=2 -XX:+DisableExplicitGC -XX:+UseTLAB -XX:-UseBiasedLocking -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -XX:+AggressiveOpts -XX:+UseCompressedOops -jar /app/littlebirds.jar --spring.config.location=/app/littlebirds.yml
