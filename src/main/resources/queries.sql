CREATE DATABASE `littlebirds`;

USE `littlebirds`;

CREATE TABLE `hourly_trending_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` int NOT NULL,
  `trending_category` varchar(100) NOT NULL,
  `trending_date` date NOT NULL,
  `trending_hour` int NOT NULL,
  `trending_item_id` bigint NOT NULL,
  `trending_item_details` json default NULL,
  `deleted` boolean DEFAULT false,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  
  PRIMARY KEY (`id`),
  INDEX `idx_country_id` (`country_id`) COMMENT 'Index on country_id column',
  INDEX `idx_country_category_date_hour` (`country_id`,`trending_category`,`trending_date`,`trending_hour`) COMMENT 'Index on country_id and other columns',
  INDEX `idx_deleted` (`deleted`) COMMENT 'Index on deleted column'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `daily_trending_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` int NOT NULL,
  `trending_category` varchar(100) NOT NULL,
  `trending_date` date NOT NULL,
  `trending_item_id` bigint NOT NULL,
  `trending_item_details` json default NULL,
  `deleted` boolean DEFAULT false,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  
  PRIMARY KEY (`id`),
  INDEX `idx_country_id` (`country_id`) COMMENT 'Index on country_id column',
  INDEX `idx_country_category_date` (`country_id`,`trending_category`,`trending_date`) COMMENT 'Index on country_id and other columns',
  INDEX `idx_deleted` (`deleted`) COMMENT 'Index on deleted column'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;